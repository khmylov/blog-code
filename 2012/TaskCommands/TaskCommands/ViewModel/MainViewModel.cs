using System.Threading;
using System.Threading.Tasks;
using GalaSoft.MvvmLight;
using TaskCommands.Commands;

namespace TaskCommands.ViewModel
{
    public class MainViewModel : ViewModelBase
    {
        private bool _isRunning;

        public bool IsRunning
        {
            get { return _isRunning; }
            set
            {
                if (_isRunning != value)
                {
                    _isRunning = value;
                    RaisePropertyChanged("IsRunning");
                }
            }
        }

        public AsyncRelayCommand StartLongOperationCommand { get; private set; }

        public MainViewModel()
        {
            StartLongOperationCommand = new AsyncRelayCommand(OnStartLongOperationAsync, () => true);
        }

        private Task OnStartLongOperationAsync()
        {
            IsRunning = true;

            return Task.Factory
                .StartNew(() => Thread.Sleep(5000))
                .ContinueWith(task =>
                {
                    // update UI

                    IsRunning = false;
                }, TaskScheduler.FromCurrentSynchronizationContext());
        }
    }
}