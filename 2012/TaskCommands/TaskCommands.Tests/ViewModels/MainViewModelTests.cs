﻿using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TaskCommands.ViewModel;

namespace TaskCommands.Tests.ViewModels
{
    [TestClass]
    public class MainViewModelTests
    {
        [TestMethod]
        public void CommandShouldNotifyAboutCompletion()
        {
            var context = new SynchronizationContext();
            SynchronizationContext.SetSynchronizationContext(context);

            var target = GetTarget();

            var task = target.StartLongOperationCommand.ExecuteAsync(null);
            task.Wait();

            // check anytihng relevant here
            Assert.IsFalse(target.IsRunning);
        }

        private MainViewModel GetTarget()
        {
            return new MainViewModel();
        }
    }
}
