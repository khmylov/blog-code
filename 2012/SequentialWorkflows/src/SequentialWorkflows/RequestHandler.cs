using System;
using System.Collections.Generic;
using System.Net;

namespace SequentialWorkflows
{
    public class RequestHandler
    {
        public IEnumerable<string> HandleSync(IEnumerable<Uri> requests)
        {
            var client = new WebClient();

            foreach (var request in requests)
            {
                yield return client.DownloadString(request);
            }
        }

        public IObservable<string> HandleAsync(IList<Uri> requests)
        {
            var client = new WebClient();

            return requests.SelectFromAsyncEventPattern
                <Uri, string, DownloadStringCompletedEventHandler,
                    DownloadStringCompletedEventArgs>(
                client.DownloadStringAsync,
                handler => (s, e) => handler(s, e),
                x => client.DownloadStringCompleted += x,
                x => client.DownloadStringCompleted -= x,
                x => x.Result,
                x => x.Error);
        }
    }
}