using System;
using System.Collections.Generic;
using System.Reactive.Subjects;

namespace SequentialWorkflows
{
    public static class ObservableExtensions
    {
        public static IObservable<TResult> SelectFromAsyncEventPattern
            <TSource, TResult, TDelegate, TEventArgs>(
            this IEnumerable<TSource> source,
            Action<TSource> run,
            Func<EventHandler<TEventArgs>, TDelegate> conversion,
            Action<TDelegate> subscribe,
            Action<TDelegate> unsubscribe,
            Func<TEventArgs, TResult> mapResult,
            Func<TEventArgs, Exception> mapException)

            where TEventArgs: EventArgs
        {
            var enumerator = source.GetEnumerator();
            var subject = new Subject<TResult>();
            var convertedHandler = default(TDelegate);

            Action takeNext = () =>
            {
                if (enumerator.MoveNext())
                {
                    run(enumerator.Current);
                }
                else
                {
                    subject.OnCompleted();
                    unsubscribe(convertedHandler);
                }
            };

            EventHandler<TEventArgs> handler = (s, e) =>
            {
                var exception = mapException(e);
                if (exception != null)
                {
                    subject.OnError(exception);
                    unsubscribe(convertedHandler);
                }
                else
                {
                    var result = mapResult(e);
                    subject.OnNext(result);
                    takeNext();
                }
            };

            convertedHandler = conversion(handler);
            subscribe(convertedHandler);

            takeNext();
            return subject;
        }
    }
}