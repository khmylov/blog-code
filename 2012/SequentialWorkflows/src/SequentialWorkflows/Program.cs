﻿using System;
using System.Linq;

namespace SequentialWorkflows
{
    public class Program
    {
        static void Main(string[] args)
        {
            var uri = new Uri("http://andweaccelerate.blogspot.com");
            var requests = Enumerable.Range(1, 5).Select(x => uri).ToArray();

            var handler = new RequestHandler();
            handler.HandleAsync(requests).Subscribe(_ => Console.WriteLine("Downloaded.."));

            Console.Read();
        }
    }
}
