Code samples for 'Async sequential workflows' blogpost.
-------------

The code shows how to compose sequential asynchronous operations with observables.

See the blogpost [here](http://khmylov.com/blog/2012/04/async-sequential-workflows/) for details.