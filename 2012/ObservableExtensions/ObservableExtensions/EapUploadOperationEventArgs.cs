using System;

namespace ObservableExtensions
{
    public class EapUploadOperationEventArgs : EventArgs
    {
        public Exception Error { get; private set; }

        public string Result { get; private set; }

        public EapUploadOperationEventArgs(string result)
        {
            Result = result;
        }

        public EapUploadOperationEventArgs(Exception exception)
        {
            Error = exception;
        }
    }
}