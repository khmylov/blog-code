﻿using System;

namespace ObservableExtensions
{
    class Program
    {
        static void Main(string[] args)
        {
            var numbers = new[] {1, 2, 3};

            var observable = Upload(numbers);

            observable.Subscribe(
                x => Console.WriteLine("Uploaded " + x), 
                () => Console.WriteLine("Completed."));

            Console.Read();
        }

        private static IObservable<string> Upload(int[] numbers)
        {
            var client = new EapClient();

            return numbers.SelectFromAsyncEventPattern
                <int, string, EapUploadOperationEventArgs>(
                    client.UploadAsync,
                    x => client.UploadCompleted += x,
                    x => client.UploadCompleted -= x,
                    x => x.Result,
                    x => x.Error);
        }
    }
}
