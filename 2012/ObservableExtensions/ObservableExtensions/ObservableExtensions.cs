using System;
using System.Collections.Generic;
using System.Reactive.Linq;
using System.Reactive.Subjects;

namespace ObservableExtensions
{
    public static class ObservableExtensions
    {
        public static IObservable<TResult> SelectFromAsyncEventPattern<TSource, TResult, TEventArgs>(
            this IEnumerable<TSource> source,
            Action<TSource> run,
            Action<EventHandler<TEventArgs>> subscribe,
            Action<EventHandler<TEventArgs>> unsubscribe,
            Func<TEventArgs, TResult> mapResult,
            Func<TEventArgs, Exception> mapException)
            where TEventArgs : EventArgs
        {
            var enumerator = source.GetEnumerator();
            if (!enumerator.MoveNext())
            {
                // we can safely return an empty stream for an empty input.
                return Observable.Empty<TResult>();
            }

            // using Subjects seems to be a nice idea
            // because we need both IObserver to provide operation results
            // and IObservable to return to the caller.
            var subject = new Subject<TResult>();

            EventHandler<TEventArgs> handler = null;
            handler = (s, e) =>
            {
                var exception = mapException(e);
                if (exception != null)
                {
                    // notify all observers about async operation error.
                    // and unsubscribe from the completion event.
                    subject.OnError(exception);
                    unsubscribe(handler);
                }

                // supply the next async operation result.
                var value = mapResult(e);
                subject.OnNext(value);

                if (enumerator.MoveNext())
                {
                    // start async operation for the next element of the input sequence.
                    run(enumerator.Current);
                }
                else
                {
                    // notify all observers that there are no more operations
                    // and unsubscribe from the completion event.
                    subject.OnCompleted();
                    unsubscribe(handler);
                }
            };

            // subscribe to the completion event.
            subscribe(handler);

            // start the async operation for the first element of the input sequence.
            run(enumerator.Current);

            return subject;
        }
    }
}