using System;
using System.Threading;

namespace ObservableExtensions
{
    public class EapClient
    {
        public event EventHandler<EapUploadOperationEventArgs> UploadCompleted;

        public void UploadAsync(int file)
        {
            ThreadPool.QueueUserWorkItem(_ =>
            {
                Console.WriteLine("Uploading " + file);

                Thread.Sleep(1000);

                var handler = UploadCompleted;
                if (handler != null)
                {
                    handler(this, new EapUploadOperationEventArgs(file.ToString()));
                }
            });
        }
    }
}