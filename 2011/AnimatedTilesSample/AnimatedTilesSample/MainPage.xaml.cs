﻿using System;
using System.Reactive.Linq;
using System.Windows;

namespace AnimatedTilesSample
{
    public partial class MainPage
    {
        // Constructor
        public MainPage()
        {
            InitializeComponent();
        }

        private void PlayAnimationHandler(object sender, RoutedEventArgs e)
        {
            Cells.Items.Clear();
            PlayAnimationButton.IsEnabled = false;

            Observable.Interval(TimeSpan.FromMilliseconds(80))
                .Take(25)
                .ObserveOnDispatcher()
                .Finally(() =>
                {
                    PlayAnimationButton.IsEnabled = true;
                })
                .Subscribe(x => Cells.Items.Add(x));
        }
    }
}