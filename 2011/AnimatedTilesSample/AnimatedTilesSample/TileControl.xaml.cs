﻿using System.Windows;
using System.Windows.Media.Animation;

namespace AnimatedTilesSample
{
    public partial class TileControl
    {
        public TileControl()
        {
            InitializeComponent();
        }

        private void OnLoaded(object sender, RoutedEventArgs e)
        {
            var sb = Resources["LoadedStoryboard"] as Storyboard;
            if (sb != null)
            {
                sb.Begin();
            }
        }
    }
}
