﻿using System;
using System.Collections.ObjectModel;
using System.IO;
using LoadManyFiles.Commands;
using LoadManyFiles.Models;

namespace LoadManyFiles.ViewModels
{
    public sealed class MainWindowViewModel: PropertyChangedNotifier
    {
        public MainWindowViewModel()
        {
            Files = new ObservableCollection<FileModel>();
        }
        
        public ObservableCollection<FileModel> Files
        {
            get;
            private set;
        }

        private DelegateCommand<String> _loadFilesCommand;
        public DelegateCommand<String> LoadFilesCommand
        {
            get
            {
                return _loadFilesCommand ?? (_loadFilesCommand = new DelegateCommand<String>(LoadFiles)
                {
                    DisableOnExecution = true,
                    IsAsync = true
                });
            }
        }

        private void LoadFiles(DelegateCommand<String> cmd, String loadFrom)
        {
            cmd.InvokeOnUIThread(() => Files.Clear());

            Action<String> addFile = file =>
            {
                var model = new FileModel
                {
                    Name = Path.GetFileNameWithoutExtension(file),
                    FullPath = Path.GetFullPath(file),
                    Extension = Path.GetExtension(file),
                    Size = GetFileSize(file)
                };
                cmd.InvokeOnUIThread(() => Files.Add(model));
            };

            Action<String> fileLoader = null;
            fileLoader = path =>
            {
                if (cmd.IsCancellationPending)
                {
                    return;
                }

                foreach (var file in Directory.GetFiles(path))
                {
                    addFile(file);
                }

                foreach (var dir in Directory.GetDirectories(path))
                {
                    if (cmd.IsCancellationPending)
                    {
                        return;
                    }

                    try
                    {
                        fileLoader(dir);
                    }
                    catch {}
                }
            };

            fileLoader(loadFrom);
        }

        private static String GetFileSize(String fileName)
        {
            try
            {
                using (var stream = File.OpenRead(fileName))
                {
                    return (stream.Length/1024).ToString();
                }
            }
            catch
            {
                return "?";
            }
        }
    }
}
