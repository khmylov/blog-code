﻿using System;

namespace LoadManyFiles.Models
{
    public class FileModel
    {
        public String Name
        {
            get;
            set;
        }

        public String Extension
        {
            get;
            set;
        }

        public String Size
        {
            get;
            set;
        }

        public String FullPath
        {
            get;
            set;
        }
    }
}
