﻿using System.Windows;
using LoadManyFiles.ViewModels;

namespace LoadManyFiles
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void StopButton_Click(object sender, RoutedEventArgs e)
        {
            (DataContext as MainWindowViewModel).LoadFilesCommand.IsCancellationPending = true;
        }
    }
}
