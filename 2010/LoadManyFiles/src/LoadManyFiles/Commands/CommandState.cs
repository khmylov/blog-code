﻿namespace LoadManyFiles.Commands
{
    public enum CommandState
    {
        Succeeded,
        Failed,
        IsExecuting
    }
}
