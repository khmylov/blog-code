﻿using System;
using System.Windows.Input;
using System.Windows.Threading;

namespace LoadManyFiles.Commands
{
    /// <summary>
    /// <see cref="ICommand"/> implementation that allows you to specify execution logic with a delegate instance.
    /// </summary>
    public sealed class DelegateCommand<T> : PropertyChangedNotifier, ICommand where T : class
    {
        private readonly Dispatcher Dispatcher;

        /// <summary>
        /// Gets or sets delegate that determines main execution logic.
        /// </summary>
        public Action<DelegateCommand<T>, T> ExecuteAction
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a predicate that determines whether a command can be executed or not.
        /// </summary>
        public Predicate<T> CanExecutePredicate
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a delegate that determines actions that will be performed before main execution logic.
        /// </summary>
        public Action<T> PreExecute
        {
            get;
            set;
        }

        /// <summary>
        /// Event that determines actions that will be performed after main execution logic.
        /// </summary>
        public event Action<DelegateCommand<T>> Executed;

        /// <summary>
        /// Gets or sets a delegate that will handle exception thrown during command execution.
        /// </summary>
        public Action<Exception> OnException
        {
            get;
            set;
        }

        public object Result
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets whether the next command execution will be performed asynchronously or not.
        /// </summary>
        public bool IsAsync
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets current command execution state.
        /// </summary>
        private CommandState _state;
        public CommandState State
        {
            get { return _state; }
            set
            {
                if (_state == value)
                {
                    return;
                }

                _state = value;

                //if command was executed asynchronously, there may be some issues with correct CanExecute checks,
                //so we force CommandManager to perform that check.
                lock (this)
                {
                    if (IsAsync && DisableOnExecution)
                    {
                        InvokeOnUIThread(CommandManager.InvalidateRequerySuggested);
                    }
                }

                OnPropertyChanged("State");
            }
        }

        private Exception _commandException;
        /// <summary>
        /// This value changes after every execution. 
        /// It will be null, if execution was successful. 
        /// Otherwise, it will contain exception that was thrown during execution.
        /// </summary>
        public Exception CommandException
        {
            get { return _commandException; }
            set
            {
                if (_commandException == value)
                {
                    return;
                }

                _commandException = value;
                OnPropertyChanged("CommandException");
            }
        }

        /// <summary>
        /// Gets or sets whether <see cref="CanExecute"/> will report False when the command is executing.
        /// Useful for asynchronous commands, when you want to disable the command button when user clicks it.
        /// </summary>
        public bool DisableOnExecution
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets whether asynchronous command should be canceled.
        /// Note that there is no automatic execution cancellation, you have to check the value of this property manually.
        /// </summary>
        public bool IsCancellationPending
        {
            get;
            set;
        }

        public DelegateCommand(Action<DelegateCommand<T>, T> execute, Predicate<T> canExecute)
        {
            ExecuteAction = execute;
            CanExecutePredicate = canExecute;

#if SILVERLIGHT
            Dispatcher = Deployment.Current.Dispatcher;
#else
            Dispatcher = Dispatcher.CurrentDispatcher;
#endif
        }

        public DelegateCommand() : this(null, null) { }

        public DelegateCommand(Action<DelegateCommand<T>, T> execute) : this(execute, null) { }

        /// <summary>
        /// Executes command
        /// </summary>
        /// <param name="parameter"></param>
        public void Execute(object parameter = null)
        {
            var typedParameter = parameter as T;

            Action action = () =>
            {
                try
                {
                    CallExecute(typedParameter);
                    if (State != CommandState.Failed)
                    {
                        State = CommandState.Succeeded;
                    }
                }
                catch (Exception ex)
                {
                    State = CommandState.Failed;
                    CommandException = ex;
                    CallOnException(ex);
                }
            };

            IsCancellationPending = false;
            CommandException = null;
            State = CommandState.IsExecuting;

            CallPreExecute(typedParameter);

            if (IsAsync)
            {
                var handler = Executed;
                if (handler == null)
                {
                    InvokeAsync(action);
                }
                else
                {
                    InvokeAsync(action, x => handler(this));
                }
            }
            else
            {
                action();
                CallExecuted();
            }
        }

        public bool CanExecute(object parameter)
        {
            if (CanExecutePredicate != null)
            {
                try
                {
                    return !GetIsBusy() && CanExecutePredicate(parameter as T);
                }
                catch
                {
                    return false;
                }
            }
            return !GetIsBusy();
        }

        private bool GetIsBusy()
        {
            return DisableOnExecution && (State == CommandState.IsExecuting);
        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public void InvokeOnUIThread(Action action)
        {
            Dispatcher.Invoke(action);
        }

        private void CallPreExecute(T arg)
        {
            if (PreExecute != null)
            {
                PreExecute(arg);
            }
        }

        private void CallExecute(T arg)
        {
            if (ExecuteAction != null)
            {
                ExecuteAction(this, arg);
            }
        }

        private void CallExecuted()
        {
            var handler = Executed;
            if (handler != null)
            {
                handler(this);
            }
        }

        private void CallOnException(Exception ex)
        {
            if (OnException != null)
            {
                InvokeOnUIThread(() => OnException(ex));
            }
        }

        private static void InvokeAsync(Action task, AsyncCallback executed)
        {
            task.BeginInvoke(executed, null);
        }

        private static void InvokeAsync(Action task)
        {
            InvokeAsync(task, x => { });
        }
    }
}