# WPF Explorer TreeView, Nov-2010

Sample app that shows how to implement explorer-like tree view in WPF.
See the blog post [here](http://khmylov.com/blog/2010/11/wpf-explorer-treeview-with-selectedpath-binding/)