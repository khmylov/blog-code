﻿using System.Windows;
using ExplorerTreeView.Controls;

namespace ExplorerTreeView
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void explorer_ExplorerError(object sender, ExplorerErrorEventArgs e)
        {
            MessageBox.Show(e.Exception.Message);
        }

        private void btnUpdate_Click(object sender, RoutedEventArgs e)
        {
            explorer.SelectedPath = txtPath.Text;
        }
    }
}
