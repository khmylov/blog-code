# XAML visual states with DataTemplates, Nov-2010

Sample Silverlight app that shows how you can switch between different visual states using data templates.
See the blog post [here](http://khmylov.com/blog/2010/11/wpf-silverlight-visual-states-with-datatemplates/)