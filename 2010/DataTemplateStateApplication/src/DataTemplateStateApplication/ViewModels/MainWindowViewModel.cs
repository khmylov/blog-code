﻿using System;
using System.ComponentModel;
using System.Threading;
using System.Windows;

namespace DataTemplateStateApplication.ViewModels
{
    public sealed class MainWindowViewModel: INotifyPropertyChanged
    {
        public const string IsBusyProperty = "IsBusy";
        private bool _isBusy;
        public bool IsBusy
        {
            get { return _isBusy; }
            set
            {
                if (_isBusy == value)
                {
                    return;
                }

                _isBusy = value;
                OnPropertyChanged(IsBusyProperty);
            }
        }

        public const string ProgressProperty = "Progress";
        private int _progress;
        public int Progress
        {
            get { return _progress; }
            set
            {
                if (_progress == value)
                {
                    return;
                }

                _progress = value;
                OnPropertyChanged(ProgressProperty);
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(String property)
        {
            var handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(property));
            }
        }

        public void StartCalculation()
        {
            IsBusy = true;
            ThreadPool.QueueUserWorkItem(_ =>
            {
                for (int i = 0; i <= 50; i++)
                {
                    int i1 = i;
                    Deployment.Current.Dispatcher.BeginInvoke(() => Progress = i1);
                    Thread.Sleep(100);
                }

                Deployment.Current.Dispatcher.BeginInvoke(() => IsBusy = false);
            });
        }
    }
}
